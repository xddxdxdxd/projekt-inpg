//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TEdit *_result;
        TButton *equal;
        TButton *one;
        TButton *four;
        TButton *seven;
        TButton *two;
        TButton *five;
        TButton *eight;
        TButton *three;
        TButton *six;
        TButton *nine;
        TButton *zero;
        TButton *additon;
        TButton *subtraction;
        TButton *multiplication;
        TButton *sinus;
        TButton *cosinus;
        TButton *divison;
        TButton *natural_logarithm;
        TButton *exponentiation;
        TButton *factorial;
        TButton *square_root;
        TButton *modulo_division;
        TButton *newton_pattern;
        TButton *logarytm;
        TButton *exponentiationresult;
        TButton *Button9;
        TButton *Button10;
        TButton *clear_button;
        TButton *coma;
        TButton *newtonpatternresult;
        TStringGrid *StringGrid1;
        TBitBtn *matrix;
        TButton *matrix_add;
        TButton *matrix_multiply;
        TListBox *ListBox1;
        TButton *ln_integral;
        TButton *ln2_integral;
        TButton *sinx_integral;
        TButton *integralcos;
        TButton *integraltan;
        TButton *inegralsqrt;
        TImage *Image1;
        void __fastcall oneClick(TObject *Sender);
        void __fastcall twoClick(TObject *Sender);
        void __fastcall threeClick(TObject *Sender);
        void __fastcall fourClick(TObject *Sender);
        void __fastcall fiveClick(TObject *Sender);
        void __fastcall sixClick(TObject *Sender);
        void __fastcall sevenClick(TObject *Sender);
        void __fastcall eightClick(TObject *Sender);
        void __fastcall nineClick(TObject *Sender);
        void __fastcall zeroClick(TObject *Sender);
        void __fastcall subtractionClick(TObject *Sender);
        void __fastcall divisonClick(TObject *Sender);
        void __fastcall additonClick(TObject *Sender);
        void __fastcall multiplicationClick(TObject *Sender);
        void __fastcall clear_buttonClick(TObject *Sender);
        void __fastcall equalClick(TObject *Sender);
        void __fastcall comaClick(TObject *Sender);
        void __fastcall sinusClick(TObject *Sender);
        void __fastcall cosinusClick(TObject *Sender);
        void __fastcall newton_patternClick(TObject *Sender);
        void __fastcall newtonpatternresultClick(TObject *Sender);
        void __fastcall logarytmClick(TObject *Sender);
        void __fastcall modulo_divisionClick(TObject *Sender);
        void __fastcall natural_logarithmClick(TObject *Sender);
        void __fastcall square_rootClick(TObject *Sender);
        void __fastcall matrixClick(TObject *Sender);
        void __fastcall ln_integralClick(TObject *Sender);
        void __fastcall ln2_integralClick(TObject *Sender);
        void __fastcall sinx_integralClick(TObject *Sender);
        void __fastcall factorialClick(TObject *Sender);
        void __fastcall integralcosClick(TObject *Sender);
        void __fastcall integraltanClick(TObject *Sender);
        void __fastcall inegralsqrtClick(TObject *Sender);
        void __fastcall exponentiationClick(TObject *Sender);
        void __fastcall exponentiationresultClick(TObject *Sender);
        void __fastcall Button9Click(TObject *Sender);
        void __fastcall Button10Click(TObject *Sender);

private:	// User declarations
public:		// User declarations
        float a,b,n,z;
        char d;
        bool przecinek;
        int e,f;
        
        __fastcall TForm1(TComponent* Owner);
};


//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
