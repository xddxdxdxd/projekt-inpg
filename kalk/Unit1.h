//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TEdit *_result;
        TButton *equal;
        TButton *one;
        TButton *four;
        TButton *seven;
        TButton *two;
        TButton *five;
        TButton *eight;
        TButton *three;
        TButton *six;
        TButton *nine;
        TButton *zero;
        TButton *additon;
        TButton *subtraction;
        TButton *multiplication;
        TButton *sinus;
        TButton *cosinus;
        TButton *divison;
        TButton *natural_logarithm;
        TButton *exponentiation;
        TButton *factorial;
        TButton *square_root;
        TButton *modulo_division;
        TButton *newton_pattern;
        TButton *Button7;
        TButton *Button8;
        TButton *AddMatrix;
        TButton *Button10;
        TButton *clear_button;
        TButton *coma;
        void __fastcall oneClick(TObject *Sender);
        void __fastcall twoClick(TObject *Sender);
        void __fastcall threeClick(TObject *Sender);
        void __fastcall fourClick(TObject *Sender);
        void __fastcall fiveClick(TObject *Sender);
        void __fastcall sixClick(TObject *Sender);
        void __fastcall sevenClick(TObject *Sender);
        void __fastcall eightClick(TObject *Sender);
        void __fastcall nineClick(TObject *Sender);
        void __fastcall zeroClick(TObject *Sender);
        void __fastcall subtractionClick(TObject *Sender);
        void __fastcall divisonClick(TObject *Sender);
        void __fastcall additonClick(TObject *Sender);
        void __fastcall multiplicationClick(TObject *Sender);
        void __fastcall clear_buttonClick(TObject *Sender);
        void __fastcall equalClick(TObject *Sender);
        void __fastcall comaClick(TObject *Sender);
        void __fastcall sinusClick(TObject *Sender);
        void __fastcall cosinusClick(TObject *Sender);

        void __fastcall modulo_divisionClick(TObject *Sender);

        

private:	// User declarations
public:		// User declarations
        float a,b;
        int e,f;
        char d;
        bool przecinek;
        __fastcall TForm1(TComponent* Owner);
};


//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
