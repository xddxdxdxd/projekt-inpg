//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <iostream>
#include <math.h>
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"


float Symnew(float x,float y)
{
        if(y==0)
        {
                return 1;
        }
        else if(x==y)
        {
                return 1;
        }
        else
        {
                return Symnew(x-1,y-1)+Symnew(x-1,y);
        }
}
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TForm1::oneClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=1;
        _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::twoClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=2;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::threeClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=3;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::fourClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=4;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::fiveClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=5;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::sixClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=6;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::sevenClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=7;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::eightClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=8;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::nineClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=9;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::zeroClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=0;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::subtractionClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='-';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::divisonClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='/';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::additonClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='+';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::multiplicationClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='*';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::clear_buttonClick(TObject *Sender)
{
        _result->Clear();
        przecinek = false;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::equalClick(TObject *Sender)
{
        if(d==' ') return;
        AnsiString s = 0;
        b=StrToFloat(_result->Text);
        f=StrToInt(_result->Text);
        switch(d)
        {
                case '+': s = FloatToStr(a+b); break;
                case '-': s = FloatToStr(a-b); break;
                case '*': s = FloatToStr(a*b); break;
                case '/': s = FloatToStr(a/b); break;
                case '%': s = FloatToStr(e%f); break;
        }
        _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::comaClick(TObject *Sender)
{
        if(!przecinek)
        {
                AnsiString temp = _result->Text;
                temp += ",";
                _result->Text=temp;
                przecinek=true;
        }


}
//---------------------------------------------------------------------------

void __fastcall TForm1::sinusClick(TObject *Sender)
{
        AnsiString s;
        a=StrToFloat(_result->Text);
        s=_result->Text;
        s+="sin(";
        _result->Text=s;
        s = FloatToStr(sin(a*M_PI/180));
         _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::cosinusClick(TObject *Sender)
{
        AnsiString s;
        a=StrToFloat(_result->Text);
        s=_result->Text;
        s+="sin(";
        _result->Text=s;
        s = FloatToStr(cos(a*M_PI/180));
         _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::newton_patternClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        _result->Text="";
        przecinek = false;


}
//---------------------------------------------------------------------------

void __fastcall TForm1::newtonpatternresultClick(TObject *Sender)
{

        b=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(Symnew(a,b));

        _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::logarytmClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(log(a));
        _result->Text=s;
        przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::modulo_divisionClick(TObject *Sender)
{
          e=StrToInt(_result->Text);
          d='%';
         _result->Text="";
         przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::natural_logarithmClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(log(a));
        _result->Text=s;
        przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::square_rootClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(sqrt(a));
        _result->Text=s;
        przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn1Click(TObject *Sender)

{
const n=11;
int i, j, suma;
int macierz[11][11];
StringGrid1->ColCount=n; //ustalenie ilosci kolumn
StringGrid1->RowCount=n; //ustalenie ilosci wierszy

for (i=0; i<=n; i++)
{
StringGrid1->Cells[i+1][0]=IntToStr(i);
StringGrid1->Cells[0][i+1]=IntToStr(i);
}

//wpisywanie 1 na przekatnej, a poza przekatna zero
for (i=0; i<n; i++)
{
for (j=0; j<n; j++)
{
if (i==j)
macierz[i][j]=1; // wpisanie 1 na przekatnej
else
macierz[i][j]=0; // wpisanie 0 poza przekatna
}
} //koniec wpisywania
//wyswietlanie tablicy
for (i=0; i<n; i++)
{
for (j=0; j<n; j++)
{
StringGrid1->Cells[i+1][j+1]=IntToStr(macierz[i][j]);
}
}
}


//------------------------------------------------------------------

//---------------------------------------------------------------------------


