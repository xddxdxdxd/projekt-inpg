object Form1: TForm1
  Left = 226
  Top = 159
  Width = 1062
  Height = 623
  Caption = '8'
  Color = clBtnFace
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Times New Roman'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 14
  object _result: TEdit
    Left = 40
    Top = 32
    Width = 441
    Height = 22
    TabOrder = 0
  end
  object equal: TButton
    Left = 496
    Top = 32
    Width = 75
    Height = 25
    Caption = '='
    TabOrder = 1
    OnClick = equalClick
  end
  object one: TButton
    Left = 40
    Top = 144
    Width = 75
    Height = 25
    Caption = '1'
    TabOrder = 2
    OnClick = oneClick
  end
  object four: TButton
    Left = 40
    Top = 176
    Width = 75
    Height = 25
    Caption = '4'
    TabOrder = 3
    OnClick = fourClick
  end
  object seven: TButton
    Left = 40
    Top = 208
    Width = 75
    Height = 25
    Caption = '7'
    TabOrder = 4
    OnClick = sevenClick
  end
  object two: TButton
    Left = 120
    Top = 144
    Width = 75
    Height = 25
    Caption = '2'
    TabOrder = 5
    OnClick = twoClick
  end
  object five: TButton
    Left = 120
    Top = 176
    Width = 75
    Height = 25
    Caption = '5'
    TabOrder = 6
    OnClick = fiveClick
  end
  object eight: TButton
    Left = 120
    Top = 208
    Width = 75
    Height = 25
    Caption = '8'
    TabOrder = 7
    OnClick = eightClick
  end
  object three: TButton
    Left = 200
    Top = 144
    Width = 75
    Height = 25
    Caption = '3'
    TabOrder = 8
    OnClick = threeClick
  end
  object six: TButton
    Left = 200
    Top = 176
    Width = 75
    Height = 25
    Caption = '6'
    TabOrder = 9
    OnClick = sixClick
  end
  object nine: TButton
    Left = 200
    Top = 208
    Width = 75
    Height = 25
    Caption = '9'
    TabOrder = 10
    OnClick = nineClick
  end
  object zero: TButton
    Left = 120
    Top = 240
    Width = 75
    Height = 25
    Caption = '0'
    TabOrder = 11
    OnClick = zeroClick
  end
  object additon: TButton
    Left = 328
    Top = 144
    Width = 75
    Height = 25
    Caption = '+'
    TabOrder = 12
    OnClick = additonClick
  end
  object subtraction: TButton
    Left = 328
    Top = 176
    Width = 75
    Height = 25
    Caption = '-'
    TabOrder = 13
    OnClick = subtractionClick
  end
  object multiplication: TButton
    Left = 328
    Top = 208
    Width = 75
    Height = 25
    Caption = '*'
    TabOrder = 14
    OnClick = multiplicationClick
  end
  object sinus: TButton
    Left = 40
    Top = 296
    Width = 75
    Height = 25
    Caption = 'sin'
    TabOrder = 15
  end
  object cosinus: TButton
    Left = 40
    Top = 328
    Width = 75
    Height = 25
    Caption = 'cos'
    TabOrder = 16
  end
  object divison: TButton
    Left = 328
    Top = 240
    Width = 75
    Height = 25
    Caption = '/'
    TabOrder = 17
    OnClick = divisonClick
  end
  object natural_logarithm: TButton
    Left = 40
    Top = 360
    Width = 75
    Height = 25
    Caption = 'ln'
    TabOrder = 18
  end
  object exponentiation: TButton
    Left = 120
    Top = 296
    Width = 75
    Height = 25
    Caption = 'x^y'
    TabOrder = 19
  end
  object factorial: TButton
    Left = 40
    Top = 392
    Width = 75
    Height = 25
    Caption = 'x!'
    TabOrder = 20
  end
  object square_root: TButton
    Left = 120
    Top = 328
    Width = 75
    Height = 25
    Caption = 'sqrroot'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 21
  end
  object modulo_division: TButton
    Left = 120
    Top = 360
    Width = 75
    Height = 25
    Caption = '%'
    TabOrder = 22
  end
  object newton_pattern: TButton
    Left = 120
    Top = 392
    Width = 75
    Height = 25
    Caption = 'combination'
    TabOrder = 23
  end
  object Button7: TButton
    Left = 200
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Button7'
    TabOrder = 24
  end
  object Button8: TButton
    Left = 200
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Button8'
    TabOrder = 25
  end
  object Button9: TButton
    Left = 200
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Button9'
    TabOrder = 26
  end
  object Button10: TButton
    Left = 200
    Top = 392
    Width = 75
    Height = 25
    Caption = 'Button10'
    TabOrder = 27
  end
  object clear_button: TButton
    Left = 496
    Top = 64
    Width = 73
    Height = 25
    Caption = 'Clear'
    TabOrder = 28
    OnClick = clear_buttonClick
  end
  object coma: TButton
    Left = 408
    Top = 144
    Width = 75
    Height = 25
    Caption = ','
    TabOrder = 29
  end
end
