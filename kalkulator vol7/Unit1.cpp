//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <iostream>
#include <math.h>
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
double f(double x)
{
        return(sin(x));
}

long double suma(long int n, double x0, double dx)
{
        long double s = 0;
        if(n > 0) s = f(x0+((double)rand()/(double)(RAND_MAX + 1)*dx)) + suma(n-1, x0, dx);
        return s;
}

long double calka(double x0, double x1)
{
        int n;
        long double s;
        double dx;
        dx = x1 - x0;
        n = 10000;
        s = dx * suma(n, x0, dx)/n;
        return s;
}

double integral(double(*f)(double x), double a, double b, int n)
{
        double step = (b - a) / n;
        double area = 0.0;
        for (int i = 0; i < n; i ++) {
                area += f(a + (i + 0.5) * step) * step; 
    }
    return area;
}
float Symnew(float x,float y)
{
        if(y==0)
        {
                return 1;
        }
        else if(x==y)
        {
                return 1;
        }
        else
        {
                return Symnew(x-1,y-1)+Symnew(x-1,y);
        }
}
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TForm1::oneClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=1;
        _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::twoClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=2;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::threeClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=3;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::fourClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=4;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::fiveClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=5;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::sixClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=6;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::sevenClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=7;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::eightClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=8;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::nineClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=9;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::zeroClick(TObject *Sender)
{
        AnsiString s;
        s=_result->Text;
        s+=0;
        _result->Text=s;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::subtractionClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='-';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::divisonClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='/';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::additonClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='+';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::multiplicationClick(TObject *Sender)
{

        a=StrToFloat(_result->Text);
        d='*';
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::clear_buttonClick(TObject *Sender)
{
        _result->Clear();
        przecinek = false;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::equalClick(TObject *Sender)
{
        if(d==' ') return;
        AnsiString s = 0;
        b=StrToFloat(_result->Text);
        f=StrToInt(_result->Text);
        switch(d)
        {
                case '+': s = FloatToStr(a+b); break;
                case '-': s = FloatToStr(a-b); break;
                case '*': s = FloatToStr(a*b); break;
                case '/': s = FloatToStr(a/b); break;
                case '%': s = FloatToStr(e%f); break;
        }
        _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::comaClick(TObject *Sender)
{
        if(!przecinek)
        {
                AnsiString temp = _result->Text;
                temp += ",";
                _result->Text=temp;
                przecinek=true;
        }


}
//---------------------------------------------------------------------------

void __fastcall TForm1::sinusClick(TObject *Sender)
{
        AnsiString s;
        a=StrToFloat(_result->Text);
        s=_result->Text;
        s+="sin(";
        _result->Text=s;
        s = FloatToStr(sin(a*M_PI/180));
         _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::cosinusClick(TObject *Sender)
{
        AnsiString s;
        a=StrToFloat(_result->Text);
        s=_result->Text;
        s+="sin(";
        _result->Text=s;
        s = FloatToStr(cos(a*M_PI/180));
         _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::newton_patternClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        _result->Text="";
        przecinek = false;


}
//---------------------------------------------------------------------------

void __fastcall TForm1::newtonpatternresultClick(TObject *Sender)
{

        b=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(Symnew(a,b));

        _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::logarytmClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(log(a));
        _result->Text=s;
        przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::modulo_divisionClick(TObject *Sender)
{
          e=StrToInt(_result->Text);
          d='%';
         _result->Text="";
         przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::natural_logarithmClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(log(a));
        _result->Text=s;
        przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::square_rootClick(TObject *Sender)
{
        a=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(sqrt(a));
        _result->Text=s;
        przecinek = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::matrixClick(TObject *Sender)

{
const n=11;
int i, j, suma;
int macierz[11][11];
StringGrid1->ColCount=n; //ustalenie ilosci kolumn
StringGrid1->RowCount=n; //ustalenie ilosci wierszy

for (i=0; i<=n; i++)
{
StringGrid1->Cells[i+1][0]=IntToStr(i);
StringGrid1->Cells[0][i+1]=IntToStr(i);
}

//wpisywanie 1 na przekatnej, a poza przekatna zero
for (i=0; i<n; i++)
{
for (j=0; j<n; j++)
{
if (i==j)
macierz[i][j]=1; // wpisanie 1 na przekatnej
else
macierz[i][j]=0; // wpisanie 0 poza przekatna
}
} //koniec wpisywania
//wyswietlanie tablicy
for (i=0; i<n; i++)
{
for (j=0; j<n; j++)
{
StringGrid1->Cells[i+1][j+1]=IntToStr(macierz[i][j]);
}
}
}


//------------------------------------------------------------------

//---------------------------------------------------------------------------


void __fastcall TForm1::ln_integralClick(TObject *Sender)
{


 double
 a,b,eps;
 int n;
 double h,w1,w2,x, i;
 a=StrToFloat(InputBox("Podaj dolna wartosc przedzialu calkowania a","",""));
 b=StrToFloat(InputBox("Podaj gorna wartosc przedzialu calkowania b","",""));
 eps=StrToFloat(InputBox("Podaj dokladnosc obliczen","",""));
 n=1;
 h=b-a;
 x=a+h/2;
 w2=log(x)+5*x;
 do
 {
        n=2*n;
        h=h/2;
        w1=w2;
        w2=0;
        x=a+h/2;
        i=1;
         do
         {
                w2=w2+log(x)+5*x;
                x=x+h;
                i=i+1;
         }
 while (i<n);
 w2=w2*h;
 }
 while (fabs(w2-w1) >= eps);
 ListBox1->Items->Clear();
 ListBox1->Items->Add("Wartosc calki dla funkcji f(x)=ln(x)+5*x");
 ListBox1->Items->Add("w przedziale a="+FloatToStrF(a,ffNumber,2,2));
 ListBox1->Items->Add("w przedziale b="+FloatToStrF(b,ffNumber,2,2));
 ListBox1->Items->Add("wyznaczona z dokladnoscia="+FloatToStr(eps));
 ListBox1->Items->Add("wynosi="+FloatToStrF(w2,ffNumber,2,2));

}
//---------------------------------------------------------------------------

void __fastcall TForm1::ln2_integralClick(TObject *Sender)
{


n=StrToFloat(InputBox("Podaj dokladnosc obliczen","",""));
a=StrToFloat(InputBox("Podaj dolna wartosc przedzialu calkowania a","",""));
b=StrToFloat(InputBox("Podaj gorna wartosc przedzialu calkowania b","",""));






ListBox1->Items->Clear();
ListBox1->Items->Add("Wartosc calki dla funkcji f(x)=ln(x)");
ListBox1->Items->Add("w przedziale a="+FloatToStr(a));
ListBox1->Items->Add("w przedziale b="+FloatToStr(b));
ListBox1->Items->Add("wynosi="+FloatToStr(integral(log, a, b, n)));

}
//---------------------------------------------------------------------------

void __fastcall TForm1::sinx_integralClick(TObject *Sender)
{
n=StrToFloat(InputBox("Podaj dokladnosc obliczen","",""));
a=StrToFloat(InputBox("Podaj dolna wartosc przedzialu calkowania a","",""));
b=StrToFloat(InputBox("Podaj gorna wartosc przedzialu calkowania b","",""));






ListBox1->Items->Clear();
ListBox1->Items->Add("Wartosc calki dla funkcji f(x)=sin(x)");
ListBox1->Items->Add("w przedziale a="+FloatToStr(a));
ListBox1->Items->Add("w przedziale b="+FloatToStr(b));
ListBox1->Items->Add("wynosi="+FloatToStr(integral(sin, a, b, n)));

}
//---------------------------------------------------------------------------

void __fastcall TForm1::factorialClick(TObject *Sender)
{
             a = StrToInt(_result->Text);
             int x = 1;
             for (int i = 1; i <= a; i++)
            {
                     x = x * i;
            }
            if (a == 0) x = 1;

        _result->Text = IntToStr(x);        
}
//---------------------------------------------------------------------------


void __fastcall TForm1::integralcosClick(TObject *Sender)
{
n=StrToFloat(InputBox("Podaj dokladnosc obliczen","",""));
a=StrToFloat(InputBox("Podaj dolna wartosc przedzialu calkowania a","",""));
b=StrToFloat(InputBox("Podaj gorna wartosc przedzialu calkowania b","",""));






ListBox1->Items->Clear();
ListBox1->Items->Add("Wartosc calki dla funkcji f(x)=cos(x)");
ListBox1->Items->Add("w przedziale a="+FloatToStr(a));
ListBox1->Items->Add("w przedziale b="+FloatToStr(b));
ListBox1->Items->Add("wynosi="+FloatToStr(integral(cos, a, b, n)));

}
//---------------------------------------------------------------------------

void __fastcall TForm1::integraltanClick(TObject *Sender)
{
n=StrToFloat(InputBox("Podaj dokladnosc obliczen","",""));
a=StrToFloat(InputBox("Podaj dolna wartosc przedzialu calkowania a","",""));
b=StrToFloat(InputBox("Podaj gorna wartosc przedzialu calkowania b","",""));






ListBox1->Items->Clear();
ListBox1->Items->Add("Wartosc calki dla funkcji f(x)=tan(x)");
ListBox1->Items->Add("w przedziale a="+FloatToStr(a));
ListBox1->Items->Add("w przedziale b="+FloatToStr(b));
ListBox1->Items->Add("wynosi="+FloatToStr(integral(tan, a, b, n)));

}
//---------------------------------------------------------------------------

void __fastcall TForm1::inegralsqrtClick(TObject *Sender)
{
n=StrToFloat(InputBox("Podaj dokladnosc obliczen","",""));
a=StrToFloat(InputBox("Podaj dolna wartosc przedzialu calkowania a","",""));
b=StrToFloat(InputBox("Podaj gorna wartosc przedzialu calkowania b","",""));






ListBox1->Items->Clear();
ListBox1->Items->Add("Wartosc calki dla funkcji f(x)=sqrt(x)");
ListBox1->Items->Add("w przedziale a="+FloatToStr(a));
ListBox1->Items->Add("w przedziale b="+FloatToStr(b));
ListBox1->Items->Add("wynosi="+FloatToStr(integral(sqrt, a, b, n)));

}
//---------------------------------------------------------------------------


void __fastcall TForm1::exponentiationClick(TObject *Sender)
{
          a=StrToFloat(_result->Text);
        _result->Text="";
        przecinek = false;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::exponentiationresultClick(TObject *Sender)
{
        b=StrToFloat(_result->Text);
        AnsiString s = 0;
        s=FloatToStr(pow(a,b));

        _result->Text=s;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button9Click(TObject *Sender)
{
        Image1->Canvas->FloodFill(1,1,0x00fefefe,fsBorder);
        Image1->Canvas->MoveTo(Image1->Width/2,Image1->Height-15);
        Image1->Canvas->LineTo(Image1->Width/2,15);
        Image1->Canvas->MoveTo(15,Image1->Height/2);
        Image1->Canvas->LineTo(Image1->Width-15,Image1->Height/2);
        Image1->Canvas->MoveTo(Image1->Width/2,Image1->Height-15);
        for (int x = 100; x < Image1->Width-100; x++ )
        {
                int y=Image1->Height-x ;
                Image1->Canvas->Pixels[x][y] = clRed;
   }
}
//---------------------------------------------------------------------------


