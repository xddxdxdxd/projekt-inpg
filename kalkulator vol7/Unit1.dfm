object Form1: TForm1
  Left = 262
  Top = 116
  Width = 1062
  Height = 637
  Caption = '8'
  Color = clBtnFace
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Times New Roman'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 14
  object Image1: TImage
    Left = 288
    Top = 280
    Width = 313
    Height = 313
  end
  object _result: TEdit
    Left = 40
    Top = 32
    Width = 441
    Height = 22
    TabOrder = 0
  end
  object equal: TButton
    Left = 496
    Top = 32
    Width = 75
    Height = 25
    Caption = '='
    TabOrder = 1
    OnClick = equalClick
  end
  object one: TButton
    Left = 40
    Top = 144
    Width = 75
    Height = 25
    Caption = '1'
    TabOrder = 2
    OnClick = oneClick
  end
  object four: TButton
    Left = 40
    Top = 176
    Width = 75
    Height = 25
    Caption = '4'
    TabOrder = 3
    OnClick = fourClick
  end
  object seven: TButton
    Left = 40
    Top = 208
    Width = 75
    Height = 25
    Caption = '7'
    TabOrder = 4
    OnClick = sevenClick
  end
  object two: TButton
    Left = 120
    Top = 144
    Width = 75
    Height = 25
    Caption = '2'
    TabOrder = 5
    OnClick = twoClick
  end
  object five: TButton
    Left = 120
    Top = 176
    Width = 75
    Height = 25
    Caption = '5'
    TabOrder = 6
    OnClick = fiveClick
  end
  object eight: TButton
    Left = 120
    Top = 208
    Width = 75
    Height = 25
    Caption = '8'
    TabOrder = 7
    OnClick = eightClick
  end
  object three: TButton
    Left = 200
    Top = 144
    Width = 75
    Height = 25
    Caption = '3'
    TabOrder = 8
    OnClick = threeClick
  end
  object six: TButton
    Left = 200
    Top = 176
    Width = 75
    Height = 25
    Caption = '6'
    TabOrder = 9
    OnClick = sixClick
  end
  object nine: TButton
    Left = 200
    Top = 208
    Width = 75
    Height = 25
    Caption = '9'
    TabOrder = 10
    OnClick = nineClick
  end
  object zero: TButton
    Left = 120
    Top = 240
    Width = 75
    Height = 25
    Caption = '0'
    TabOrder = 11
    OnClick = zeroClick
  end
  object additon: TButton
    Left = 328
    Top = 144
    Width = 75
    Height = 25
    Caption = '+'
    TabOrder = 12
    OnClick = additonClick
  end
  object subtraction: TButton
    Left = 328
    Top = 176
    Width = 75
    Height = 25
    Caption = '-'
    TabOrder = 13
    OnClick = subtractionClick
  end
  object multiplication: TButton
    Left = 328
    Top = 208
    Width = 75
    Height = 25
    Caption = '*'
    TabOrder = 14
    OnClick = multiplicationClick
  end
  object sinus: TButton
    Left = 40
    Top = 296
    Width = 75
    Height = 25
    Caption = 'sin'
    TabOrder = 15
    OnClick = sinusClick
  end
  object cosinus: TButton
    Left = 40
    Top = 328
    Width = 75
    Height = 25
    Caption = 'cos'
    TabOrder = 16
    OnClick = cosinusClick
  end
  object divison: TButton
    Left = 328
    Top = 240
    Width = 75
    Height = 25
    Caption = '/'
    TabOrder = 17
    OnClick = divisonClick
  end
  object natural_logarithm: TButton
    Left = 40
    Top = 360
    Width = 75
    Height = 25
    Caption = 'ln'
    TabOrder = 18
    OnClick = natural_logarithmClick
  end
  object exponentiation: TButton
    Left = 120
    Top = 296
    Width = 75
    Height = 25
    Caption = 'x^y'
    TabOrder = 19
    OnClick = exponentiationClick
  end
  object factorial: TButton
    Left = 40
    Top = 392
    Width = 75
    Height = 25
    Caption = 'x!'
    TabOrder = 20
    OnClick = factorialClick
  end
  object square_root: TButton
    Left = 120
    Top = 328
    Width = 75
    Height = 25
    Caption = 'sqrroot'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 21
    OnClick = square_rootClick
  end
  object modulo_division: TButton
    Left = 120
    Top = 360
    Width = 75
    Height = 25
    Caption = '%'
    TabOrder = 22
    OnClick = modulo_divisionClick
  end
  object newton_pattern: TButton
    Left = 120
    Top = 392
    Width = 75
    Height = 25
    Caption = 'combination'
    TabOrder = 23
    OnClick = newton_patternClick
  end
  object logarytm: TButton
    Left = 200
    Top = 328
    Width = 75
    Height = 25
    Caption = 'log'
    TabOrder = 24
    OnClick = logarytmClick
  end
  object exponentiationresult: TButton
    Left = 200
    Top = 296
    Width = 75
    Height = 25
    Caption = 'x^y result'
    TabOrder = 25
    OnClick = exponentiationresultClick
  end
  object Button9: TButton
    Left = 200
    Top = 360
    Width = 75
    Height = 25
    Caption = 'y=x'
    TabOrder = 26
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 200
    Top = 392
    Width = 75
    Height = 25
    Caption = 'Button10'
    TabOrder = 27
  end
  object clear_button: TButton
    Left = 496
    Top = 64
    Width = 73
    Height = 25
    Caption = 'Clear'
    TabOrder = 28
    OnClick = clear_buttonClick
  end
  object coma: TButton
    Left = 200
    Top = 240
    Width = 75
    Height = 25
    Caption = ','
    TabOrder = 29
    OnClick = comaClick
  end
  object newtonpatternresult: TButton
    Left = 104
    Top = 424
    Width = 105
    Height = 41
    Caption = 'combinatrion result'
    TabOrder = 30
    OnClick = newtonpatternresultClick
  end
  object StringGrid1: TStringGrid
    Left = 616
    Top = 32
    Width = 320
    Height = 120
    TabOrder = 31
  end
  object matrix: TBitBtn
    Left = 944
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 32
    OnClick = matrixClick
  end
  object matrix_add: TButton
    Left = 616
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 33
  end
  object matrix_multiply: TButton
    Left = 712
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Multiply'
    TabOrder = 34
  end
  object ListBox1: TListBox
    Left = 608
    Top = 256
    Width = 225
    Height = 137
    ItemHeight = 14
    TabOrder = 35
  end
  object ln_integral: TButton
    Left = 848
    Top = 392
    Width = 89
    Height = 25
    Caption = 'integral ln(x)+5x'
    TabOrder = 36
    OnClick = ln_integralClick
  end
  object ln2_integral: TButton
    Left = 848
    Top = 360
    Width = 89
    Height = 25
    Caption = 'integral ln(x)'
    TabOrder = 37
    OnClick = ln2_integralClick
  end
  object sinx_integral: TButton
    Left = 848
    Top = 232
    Width = 89
    Height = 25
    Caption = 'integral sin(x)'
    TabOrder = 38
    OnClick = sinx_integralClick
  end
  object integralcos: TButton
    Left = 848
    Top = 264
    Width = 89
    Height = 25
    Caption = 'integral cos(x)'
    TabOrder = 39
    OnClick = integralcosClick
  end
  object integraltan: TButton
    Left = 848
    Top = 296
    Width = 89
    Height = 25
    Caption = 'integral tan(x)'
    TabOrder = 40
    OnClick = integraltanClick
  end
  object inegralsqrt: TButton
    Left = 848
    Top = 328
    Width = 89
    Height = 25
    Caption = 'integral sqrt(x)'
    TabOrder = 41
    OnClick = inegralsqrtClick
  end
end
